# Rundeck running locally #

Simple tutorial that show how to up a virtual machine with Rundeck provisioned by ansible running locally.

### Requirements ###

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

## Simple Demo ##

![vagrant_rundeck](https://media.giphy.com/media/5QKwxYPq1lYT9W6nL2/giphy.gif)